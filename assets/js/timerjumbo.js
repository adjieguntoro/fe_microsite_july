
$(function(){
    var fullDate = new Date();
    var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
	var twoDigitDay = ((fullDate.getDate().length+1) === 1)? (fullDate.getDate()+1) : '0' + (fullDate.getDate()+1)
    var currentDate =  fullDate.getFullYear()  + "-" + twoDigitMonth + "-" + twoDigitDay;
    var ramadhanDate = '2017-05-26';
    var days_between = days_between(ramadhanDate, currentDate);
    // console.log("serial Date : " + days_between);
	var serial_date = days_between;
    var magrib_time = '';

    // Tomorrow Date
    var tomorrow = new Date(fullDate.getTime() + (24 * 60 * 60 * 1000));
    var tomtwoDigitMonth = ((tomorrow.getMonth().length+1) === 1)? (tomorrow.getMonth()+1) : '0' + (tomorrow.getMonth()+1);
    var tommorowDate = tomorrow.getFullYear() + '-' + tomorrow.getDate() + '-' + tomorrow.getDate();
    // console.log(tommorowDate);

    var currentDateForMagribTimer = currentDate;
    var currentTime = fullDate.getTime();
    var magribFullDate = new Date(get_magrib_time()['date'] + ' ' + get_magrib_time()['time']);
    var magribTime = magribFullDate.getTime();
    // console.log(currentTime);
    // console.log(magribTime);

    if (currentTime >= magribTime + 3600000) {
        currentDateForMagribTimer = tommorowDate;
    }
    
	
	// console.log(serial_date);

    // Get Magrib time :

    function get_magrib_time(){
        $.ajax({
            type: "GET",
	 		url : "http://103.248.56.25/api/v1/magrib/" + serial_date,
	 		async: false,
	 		success:function(data){
	 			 data_ = data.data[0];
                 magrib_time = [];
                 magrib_time['time'] = data_.attributes.time;
                 magrib_time['date'] = data_.attributes.date;
                 
                //  console.log(magrib_time);
	 		    },
	 		error:function(err, Status) {
	 			console.log("Terjadi Kesalahan");
	 		    }
        });
        return magrib_time;
    }

    function get_magrib_tomorrow(){
        
        $.ajax({
        
            type: "GET",
	 		url : "http://103.248.56.25/api/v1/magrib/" + parseInt(serial_date + 1),
	 		async: false,
	 		success:function(data){
                 magrib_time_t = [];
	 			 data_ = data.data[0];
                 magrib_time_t['date'] = data_['attributes']['date'];
                 magrib_time_t['time'] = data_['attributes']['time'];
                 
                 
                //  console.log(data_);
	 		    },
	 		error:function(err, Status) {
	 			console.log("Terjadi Kesalahan");
	 		    }
        });
        return magrib_time_t;
    }

	function days_between(date1, date2) {
      // Here are the two dates to compare
        // var date1 = '2017-04-25';
        // var date2 = '2017-05-28';

        // First we split the values to arrays date1[0] is the year, [1] the month and [2] the day
        date1 = date1.split('-');
        date2 = date2.split('-');

        // Now we convert the array to a Date object, which has several helpful methods
        date1 = new Date(date1[0], date1[1], date1[2]);
        date2 = new Date(date2[0], date2[1], date2[2]);

        // We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
        date1_unixtime = parseInt(date1.getTime() / 1000);
        date2_unixtime = parseInt(date2.getTime() / 1000);

        // This is the calculated difference in seconds
        var timeDifference = date2_unixtime - date1_unixtime;

        // in Hours
        var timeDifferenceInHours = timeDifference / 60 / 60;

        // and finaly, in days :)
        var timeDifferenceInDays = timeDifferenceInHours  / 24;

        return timeDifferenceInDays;
    }
    function timer(settings){
        var config = {
           endDate: '2017-06-25 00:00',
            timeZone: 'Asia/Jakarta',
            hours: $('.hours'),
            minutes: $('.minutes'),
            seconds: $('.seconds'),
            newSubMessage: 'and should be back online in a few minutes...'
        };
        function prependZero(number){
            return number < 10 ? '0' + number : number;
        }
        $.extend(true, config, settings || {});
        var currentTime = moment();
        var endDate = moment.tz(config.endDate, config.timeZone);
        var diffTime = endDate.valueOf() - currentTime.valueOf();
        var duration = moment.duration(diffTime, 'milliseconds');
        // console.log(duration)
        var days = duration.days();
        var months = duration.months();
        var interval = 1000;
        var subMessage = $('.sub-message');
        var clock = $('.clock');
        if(diffTime < 0){
            endEvent(subMessage, config.newSubMessage, clock);
            return;
        }

        if(months > 0){
            days = 31 + days;
        }
        if(days > 0){
            $('.days').text(prependZero(days));
        }

        

        var intervalID = setInterval(function(){
            duration = moment.duration(duration - interval, 'milliseconds');
            var hours = duration.hours(),
                minutes = duration.minutes(),
                seconds = duration.seconds();
            days = duration.days();
            if(hours  <= 0 && minutes <= 0 && seconds  <= 0 && days <= 0){
                seconds=0;
                

            }
            if(days === 0){

            }

            if(months > 0){
                days = 31+ days;
            }
            $('.days').text(prependZero(days));

            config.hours.text(prependZero(hours));
            config.minutes.text(prependZero(minutes));
            config.seconds.text(prependZero(seconds));
        }, interval);


    }
   
   
   

   function mgb_timer(settings){
        var config = {
            endDate: currentDateForMagribTimer + ' ' + get_magrib_time()['time'],
            timeZone: 'Asia/Jakarta',
            hours: $('.mgb_hours'),
            minutes: $('.mgb_minutes'),
            seconds: $('.mgb_seconds'),
            newSubMessage: 'and should be back online in a few minutes...'
        };
        function prependZero(number){
            return number < 10 ? '0' + number : number;
        }
        $.extend(true, config, settings || {});
        var currentTime = moment();
        var endDate = moment.tz(config.endDate, config.timeZone);
        var diffTime = endDate.valueOf() - currentTime.valueOf();
        var duration = moment.duration(diffTime, 'milliseconds');
        var days = duration.days();
        var interval = 1000;
        var subMessage = $('.sub-message');
        var clock = $('.clock');
        if(diffTime < 0) {
            endEvent(subMessage, config.newSubMessage, clock);
            config.hours.text('00');
            config.minutes.text(prependZero('0'));
            config.seconds.text(prependZero('0'));
            showMsgBerbuka()
            return;
        }
        if(days > 0){
            $('.mgb_days').text(prependZero(days));
        }
        var intervalID = setInterval(function(){
            duration = moment.duration(duration - interval, 'milliseconds');
            var hours = duration.hours(),
                minutes = duration.minutes(),
                seconds = duration.seconds();
            days = duration.days();
            if(hours  <= 0 && minutes <= 0 && seconds  <= 0 && days <= 0){
                seconds=0;
                showMsgBerbuka()
            }
            if(days === 0){

            }
            $('.mgb_days').text(prependZero(days));

            config.hours.text(prependZero(hours));
            config.minutes.text(prependZero(minutes));
            config.seconds.text(prependZero(seconds));
        }, interval);


    }

    function endEvent($el, newText, hideEl){
        $el.text(newText);
        hideEl.hide();
   }

   function showMsgBerbuka(){
    //    console.log("selamat berbuka");
        $('#magrib_count_down').addClass('hide');
        //$('#msg-berbuka').removeClass('hide');
        $('#msg-berbuka').addClass('zoomIn');
   }

    timer();
    mgb_timer();

    
    
});
